FROM php:8.0-apache

COPY ./ /var/www/html

LABEL CREATOR = "Isis Oliveira"

EXPOSE 80

ENV environment = "development"s

CMD ["apache2-foreground"]